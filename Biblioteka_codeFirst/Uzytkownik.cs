﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Biblioteka_codeFirst
{
    [Table("Uzytkownik")]
    public class Uzytkownik
    {
        [Key]
        public int UzytkownikID { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Adres { get; set; }
        public long PESEL { get; set; }
        public string Email { get; set; }

        public List<Ksiazka> Ksiazki { get; set; }
    }
}