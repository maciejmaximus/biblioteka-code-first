﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biblioteka_codeFirst
{
    public class BibliotekaFunkcje
    {

        public List<Ksiazka> WyswietlKsiazki()
        {
            BibliotekaDBContext bibliotekaDBContext = new BibliotekaDBContext();
            return bibliotekaDBContext.Ksiazki.ToList();
        }

    }
}