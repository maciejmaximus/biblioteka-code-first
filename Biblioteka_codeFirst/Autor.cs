﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Biblioteka_codeFirst
{
    [Table("Autor")]
    public class Autor
    {
        [Key]
        public int AutorID { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }

        public List<Ksiazka> Ksiazki { get; set; }
    }
}