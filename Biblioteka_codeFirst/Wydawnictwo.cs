﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Biblioteka_codeFirst
{
    [Table("Wydawnictwo")]
    public class Wydawnictwo
    {
        [Key]
        public int WydawnictwoID { get; set; }
        public string NazwaWydawnictwa { get; set; }

        public List<Ksiazka> Ksiazki { get; set; }
    }
}