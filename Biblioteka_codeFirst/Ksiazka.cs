﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Biblioteka_codeFirst
{
    [Table("Ksiazka")]
    public class Ksiazka
    {
        //[Key]
        public int ID { get; set; }
        public string Tytul { get; set; }

        public int AutorID { get; set; }
        [ForeignKey("AutorID")]
        public Autor Autor { get; set; }
        //public int GatunekID { get; set; }
        //[ForeignKey("GatunekID")]
        //public Gatunek Gatunek { get; set; }
        public int WydawnictwoID { get; set; }
        [ForeignKey("WydawnictwoID")]
        public Wydawnictwo Wydawnictwo { get; set; }
        //public int UzytkownikID { get; set; }
        //[ForeignKey("UzytkownikID")]
        //public Uzytkownik Uzytkownik { get; set; }

    }
}