﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Biblioteka_codeFirst
{
    public class BibliotekaDBContext : DbContext
    {
        public DbSet<Autor> Autorzy { get; set; }
        //public DbSet<Gatunek> Gatunki { get; set; }
        public DbSet<Wydawnictwo> Wydawnictwa { get; set; }
        //public DbSet<Uzytkownik> Uzytkownicy { get; set; }
        public DbSet<Ksiazka> Ksiazki { get; set; }
    }
}
