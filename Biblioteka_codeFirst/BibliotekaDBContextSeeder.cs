﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Biblioteka_codeFirst
{
    public class BibliotekaDBContextSeeder : DropCreateDatabaseIfModelChanges<BibliotekaDBContext>
    {
        protected override void Seed(BibliotekaDBContext context)
        {
            //context.Gatunki.Add(new Gatunek() { GatunekID = 1, NazwaGatunku = "Przygodowe" });
            //context.Gatunki.Add(new Gatunek() { GatunekID = 2, NazwaGatunku = "Fantasy" });
            //context.Gatunki.Add(new Gatunek() { GatunekID = 3, NazwaGatunku = "Naukowe" });

            Autor autor1 = new Autor()
            {
                Imie = "Henryk",
                Nazwisko = "Sienkiewicz"
            };
            Autor autor2 = new Autor()
            {
                Imie = "Wisława",
                Nazwisko = "Szymborska"
            };
            Autor autor3 = new Autor()
            {
                Imie = "Andrzej",
                Nazwisko = "Sapkowski"
            };
            Autor autor4 = new Autor()
            {
                Imie = "Julian",
                Nazwisko = "Tuwim"
            };

            context.Autorzy.Add(autor1);
            context.Autorzy.Add(autor2);
            context.Autorzy.Add(autor3);
            context.Autorzy.Add(autor4);

            //Gatunek gatunek1 = new Gatunek()
            //{
            //    NazwaGatunku = "Przygodowe"
            //};
            //Gatunek gatunek2 = new Gatunek()
            //{
            //    NazwaGatunku = "Fantasy"
            //};
            //Gatunek gatunek3 = new Gatunek()
            //{
            //    NazwaGatunku = "Naukowe"
            //};

            //context.Gatunki.Add(gatunek1);
            //context.Gatunki.Add(gatunek2);
            //context.Gatunki.Add(gatunek3);

            Wydawnictwo wydawnictwo1 = new Wydawnictwo()
            {
                NazwaWydawnictwa = "PWN",
            };
            Wydawnictwo wydawnictwo2 = new Wydawnictwo()
            {
                NazwaWydawnictwa = "Znak",
            };
            Wydawnictwo wydawnictwo3 = new Wydawnictwo()
            {
                NazwaWydawnictwa = "Orle Pióro",
            };

            context.Wydawnictwa.Add(wydawnictwo1);
            context.Wydawnictwa.Add(wydawnictwo2);
            context.Wydawnictwa.Add(wydawnictwo3);

            //Uzytkownik uzytkownik1 = new Uzytkownik()
            //{
            //    Imie = "Maciej",
            //    Nazwisko = "Pesz",
            //    Adres = "ul. Malinowa 5, 65-543 Poznań",
            //    PESEL = 12347890876,
            //    Email = "maciej.pe@onet.eu"
            //};
            //Uzytkownik uzytkownik2 = new Uzytkownik()
            //{
            //    Imie = "Adam",
            //    Nazwisko = "Nowak",
            //    Adres = "ul. Koniakowa 52, 23-333 Swarzędz",
            //    PESEL = 33345666777,
            //    Email = "Adam.ne@interia.eu"
            //};
            //Uzytkownik uzytkownik3 = new Uzytkownik()
            //{
            //    Imie = "Iwona",
            //    Nazwisko = "Kowalska",
            //    Adres = "ul. Wiejska 2, 43-989 Luboń",
            //    PESEL = 22343432111,
            //    Email = "ivo.kowalska@o2.pl"
            //};

            //context.Uzytkownicy.Add(uzytkownik1);
            //context.Uzytkownicy.Add(uzytkownik2);
            //context.Uzytkownicy.Add(uzytkownik3);

            Ksiazka ksiazka1 = new Ksiazka()
            {
                Tytul = "Potop",
                AutorID = 1,
                //GatunekID = 1,
                //WydawnictwoID = 1,
                //UzytkownikID = 3
            };
            Ksiazka ksiazka2 = new Ksiazka()
            {
                Tytul = "Fraszka",
                AutorID = 2,
                //GatunekID = 3,
                //WydawnictwoID = 2,
                //UzytkownikID = 1
            };
            Ksiazka ksiazka3 = new Ksiazka()
            {
                Tytul = "Lokomotywa",
                AutorID = 4,
                //GatunekID = 2,
                //WydawnictwoID = 3,
                //UzytkownikID = 2
            };
            Ksiazka ksiazka4 = new Ksiazka()
            {
                Tytul = "Robot",
                AutorID = 3,
                //GatunekID = 2,
                //WydawnictwoID = 3,
                //UzytkownikID = 2
            };
            Ksiazka ksiazka5 = new Ksiazka()
            {
                Tytul = "Wierszyk",
                AutorID = 2,
                //GatunekID = 2,
                //WydawnictwoID = 2,
                //UzytkownikID = 3
            };
            Ksiazka ksiazka6 = new Ksiazka()
            {
                Tytul = "Ogniem i mieczem",
                AutorID = 1,
                //GatunekID = 1,
                //WydawnictwoID = 1,
                //UzytkownikID = 1
            };
            Ksiazka ksiazka7 = new Ksiazka()
            {
                Tytul = "Do prostego człowieka",
                AutorID = 4,
                //GatunekID = 3,
                //WydawnictwoID = 2,
                //UzytkownikID = 1
            };

            context.Ksiazki.Add(ksiazka1);
            context.Ksiazki.Add(ksiazka2);
            context.Ksiazki.Add(ksiazka3);
            context.Ksiazki.Add(ksiazka4);
            context.Ksiazki.Add(ksiazka5);
            context.Ksiazki.Add(ksiazka6);
            context.Ksiazki.Add(ksiazka7);

            base.Seed(context);
        }

    }
}