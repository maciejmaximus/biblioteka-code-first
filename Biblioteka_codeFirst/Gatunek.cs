﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Biblioteka_codeFirst
{
    [Table("Gatunek")]
    public class Gatunek
    {
        [Key]
        public int GatunekID { get; set; }
        public string NazwaGatunku { get; set; }

        public List<Ksiazka> Ksiazki { get; set; }
    }
}